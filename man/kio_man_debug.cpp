/*  This file is part of the KDE project
    SPDX-FileCopyrightText: 2014 Laurent Montel <montel@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "kio_man_debug.h"
Q_LOGGING_CATEGORY(KIO_MAN_LOG, "log_kio_man")


